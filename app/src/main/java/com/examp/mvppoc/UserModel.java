package com.examp.mvppoc;

/**
 * Created by CK Sharma on 9/20/2018.
 */

public class UserModel {

    private String fullname="";
    private String email="";

    public UserModel(){

    }

    public UserModel(String fullname,String email){
        this.fullname=fullname;
        this.email=email;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    @Override
    public String toString(){
        return "Email : " + email + "\nFullName : " + fullname;
    }
}
