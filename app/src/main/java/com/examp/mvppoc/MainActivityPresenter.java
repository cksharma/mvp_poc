package com.examp.mvppoc;


/**
 * Created by CK Sharma on 9/21/2018.
 */

public class MainActivityPresenter {
    private UserModel user;
    private View view;

    public MainActivityPresenter(View view){
        this.user=new UserModel();
        this.view=view;
    }
    public void updateFullName(String fullname){
        user.setFullname(fullname);
        view.updateUserInfoTextView(user.toString());
    }
    public void updateEmail(String email){
        user.setEmail(email);
        view.updateUserInfoTextView(user.toString());
    }

    public interface View{

        void updateUserInfoTextView(String info);
        void showProgressBar();
        void hideProgressBar();

    }
}
